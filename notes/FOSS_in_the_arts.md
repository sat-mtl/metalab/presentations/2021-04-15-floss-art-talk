# FOSS in the Arts

What is the role of open source software in the arts?
Do tools shape/inform/influence artistic practice? ("Medium is the message", among other things?)

Using free software is important to artists working in digital domain. It provides the [flexibility necessary to manipulate the digital media](technological_arts_and_digital_literacy.md). In the case of those digital/technological artists who are also [end-user programmers](end_user_programming.md), open-source software will serve them best.

There are different levels of digital literacy. Ability to manipulate digital media with a programming language is one of the very valuable skills. Those who have that ability will see a greater value in Free/Libre than proprietary software.

-   McLuhan, M., National Broadcasting Company., & McGraw-Hill Book Company. (1967). This is Marshall McLuhan: The medium is the message. New York: NBC.
-   Kilian, A. (). Defining Digital Space Through a Visual Language. ,  (), 118.
-   Seta, Michal. “Freedom Squared: Free Improvisation in the Free Software World.” eContact! 11.3 — Logiciels audio « open source » / Open Source for Audio Application (September 2009).
