
# Technological arts and digital literacy


-   **tags:** [End-user programming](end_user_programming.md) [Bricolage Programming](bricoleur_programmer.md)

    We could probably safely assume that someone who is involved in the craft of technological arts is [digitally literate?](https://en.wikipedia.org/wiki/Digital_literacy). Anyone who wants to create an artefact of digital media will need to use some tool made for that purpose. Axel   Kilian considers programmability as the biggest potential of digital medium. It is a property of the medium but is not present in the digital information itself. Programming is a way of interfacing with the digital medium in its own language. Programming languages are a two-way communication devices that allow describing processes in human readable and machine readable terms at once. In that sense, it may be in the best interest of a digital/technological artist to know one or more programming languages to manipulate the digital medium.

-   Kilian, A. (). Defining Digital Space Through a Visual Language. ,  (), 118.
