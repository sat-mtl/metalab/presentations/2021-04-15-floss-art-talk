# La SAT et le libre

## Historique SAT: les logiciels du Metalab, histoire, TI, artistes

Le Hub urbain de la SAT. Vision, préoccupations, possibilités et orientation futures. Husband, J.; and Barsalo, R. SAT Metalab White Paper, 2005.
"Le cœur du concept de hub urbain que la SAT est en voie de mettre en place consiste à offrir une plateforme ouverte et évolutive qui réunit des logiciels d’avant-garde (essentiellement en code source libre), des technologies numériques et des ressources qui permettent la création, la diffusion et la distribution de contenu culturel numérique."

Depuis au moins 2005, le Metalab produit des logiciels libres pour les arts numériques : télépresence, immersion visuelle, spatialisation du son, création de contenu et interaction

Publication de contenu artistique sous licence Creative Commons :
- Dataset concert symphonique de l'OSM

La SAT contribue à des outils open source, dans les deux dernières années :
- Blender (modélisation 3D, addons)
- Pyrr (librairie mathématique pour le langage Python)
- Supercollider (langage de programmation dédié au son, ajout du logiciel SATIE)
- SC-HOA (librairie pour le support de l'ambisonie dans le  appelé Supercollider)
- Mesa graphics drivers
- PyOpenPose (détection de mouvement)
- FAUST (langage de programmation audio)

La SAT utilise des outils libre
- Infrastructure TI
- Web


## Le Metalab et résidence de recherche

Les résidences artistiques, avec des logiciels libre :
### Contamine (2008-2014)

https://sat.qc.ca/fr/residences/contamine

"Partie intégrante du département de résidences de la SAT, le projet Contamine est un programme d’échanges artistiques nationaux et internationaux ayant pour objectif de réaliser des résidences croisées impliquant la recherche et la création multidisciplinaire."

Les résidences "contamine" :
- Mons 2008-2011 https://sat.qc.ca/fr/evenements/soupe-transatlantique
- Poitiers 2011-2012 https://sat.qc.ca/fr/nouvelles/lucifuge
- Taîwan 2012 https://archives.sat.qc.ca/fr/content/contamine-taiwan-2012
- Liverpool-FACT 2012-2013 https://sat.qc.ca/fr/evenements/lespace-dessiner-drawing-space
- Rennes 2013-2014 https://sat.qc.ca/fr/miscible https://sat.qc.ca/fr/albums/miscible


### Scènes ouvertes

[Scènes ouvertes](https://sat.qc.ca/fr/scenic-telepresence), 20+ lieux dans le réseau de télépresence pour environ 10 projets de téléprésence par an. Par exemple [Bluff](https://vimeo.com/347788736).

https://sat.qc.ca/fr/nouvelles/evenements-en-telepresence-scenique-0

### Autre résidences & travaux artistique

- 2011 [INTACT, Just In Case](http://saramalinarich.net/en/just-in-case-2/)
- 2013 Dieu est un DJ 
- 2014 Réseau natif Création SAT de téléprésence entre dômes (SAT et India American Institute for Arts de Santa Fe) [image](../Metalab-slides/img/scenic-IAIA-2014-IX.jpg)
- 2014 Turcot https://sat.qc.ca/fr/evenements/turcot-la-route-devenue-architecture
- 2016 Aquakhoria (SATIE) https://sat.qc.ca/en/evenements/aqua-khoria
- 2021 Molécule / -22.7 (en cours)

Michal Seta:
- 2014 Re-Collect http://janetingley.com/re-collect/ (SPIN)
- 2019 Fadeferra http://djiamnot.xyz/pages/works/Fadeferra.html (SATIE)
- 2020 mimoidalaube https://vimeo.com/483783290 (SATIE)
- Okta http://oktaproject.ca/introduction/ (SPIN) (œuvre publique, installation permanente)

## Action possibles de la SAT: communication, valorisation, communauté

- Créer un programme de résidence de recherche autour des outils du Metalab, le résidences de recherche se construisent (et ce sont construites) par nos réseaux uniquement
- Mettre en avant la position active de la SAT vis-à-vis des FLOSS dans ses textes de présentation, ses valeurs et ses actions
- Mettre en place un axe d'enseignement orienté FLOSS à Campus, incluant des introductions au libre et les outils du Metalab

Actions de la part du Metalab, relative à la recherche uniquement et sa valorisation
- livre blanc sur le libre relativement aux recherches du Metalab
- formalisation d'une offre de service de recherche autour des logiciels libres du Metalab suivant l'étude produite par le Metalab et l'ETS en août 2020

