#### La "Architecture Machine" au 21ème siècle

Dans les années 70 le MIT a démarré un projet d'informatique pour l'architecture. Son directeur en parle dans ce livre (en accès libre): https://direct.mit.edu/books/book/5049/The-Architecture-MachineToward-a-More-Human

Un article résume le projet: https://medium.com/designscience/1973-a1b835e87d1c

L'architecture étant cet art (premier) qui structure les sociétés, traditionnellement conçu et fait "à la main" (jusqu'alors sans l'apport de technologies numériques), c'était alors un projet très innovateur. L'intelligence artificielle en était partie intégrante. 

Mais c'était l'époque où Internet n'existait pas et où l'émergence d'une "intelligence collective numérique" n'était considérée que de manière prospective, surtout en fonction de ses aspects non-numériques en tant qu'architecture "indigène" (voir le chapitre "Computer-Aided Participatory Design").

L'informatique et l'industrie en général étant (encore aujourd'hui) paternaliste, la techno-science s'évertue à offrir des "solutions" bien enrobées qui laissent peu de place à l'intelligence individuelle et collective, sauf pour du bricolage (et encore)...

L'idée d'un design participatif médiatisé par l'informatique suppose que le dialogue humain-machine ne se limite plus à l'interaction d'un invididu avec une machine mais que des humains interagissent par l'entreprise (ou la participation) de machines en réseaux. Dans un tel cas de figure, "l'art" (vu comme mode de production) devient une activité collective, plus seulement une appréciation d'une foule du travail d'individus atomisés.

Aujourd'hui, avec la place importante qu'ont pris les logiciels "libres et open source" au sein de l'industrie informatique (qui est présente dans presques tous les domaines d'activités humaine), il est important d'en tenir compte à tous les niveaux des processus créatifs qui structurent nos existances. Comme la SAT s'insère en tant qu'institution qui fédère la mouvance du numérique dans l'art, elle a tout intérêt à considérer les logiciels et la culture libre dans son projet, de manière structurante et engagée.
