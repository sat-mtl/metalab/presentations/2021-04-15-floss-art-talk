# Statue of Anne (Copyright Act) 1710

Nommée après la reine de la Grande Bretagne de l'époque, est une loi du Parlement qui est la première qui permet de regulaiser le copyright par le gouvernement au lieu de l'accord entre les individus. Cette loi permet à l'auteur d'obtenir le droit exclusif d'impression de son oeuvre et est le premier élément de droit partimonial sur les oeuvres artistiques.

Avant cette date, Stationers' Company avait le monopole sur l'impression (et la responsabilité de la censure) des écrits.

Cette loi fixait le droit d'auteur à une durée 14 ans pour les ouvrages nouvellement édités, l'oeuvre passait à la domaine publique par la suite.
