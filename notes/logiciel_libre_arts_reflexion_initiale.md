# Le libre et les arts

## Commençons par quelques exemples...

## Outils libres utilisés à la SAT et au Metalab

* Linux
* Blender
* Python
* SuperCollider
* ffmpeg

## Outils libres utilisés en art

* Glimpse
* Csound
* Ardour
* PureData
* Audacity


## Alors, qu'est-ce que le libre ?

Un mouvement ?

Une philosophie ?

Une vision du monde ?

## Ce qu'on veut dire par FLOSS

FLOSS: Free, Libre, Open Source Software

## Free

Ici, il est question de gratuité. 

Le logiciel est accessible à tout le monde peu importe leur budget car il est *gratuit*.

## Libre

Ici, il est question de liberté.

Les personnes utilisant le logiciel sont libres de se l'approprier, par exemple en effectuant les changements qu'elles veulent, en ajoutant des fonctionnalités, [autres exemples concrets].

## Libre

Cette liberté est valide pour toutes les personnes souhaitant utiliser le logiciel.

Dans certaines licences [lesquelles ?], cette liberté s'étend aux nouvelles contributions à ce logiciel.

[est-ce similaire ou pareil à la notion de share alike dans CC ?]

## Open Source

Open Source: il faut que le code source soit ouvert, c'est-à-dire disponible à toute personne qui en fait la demande.

## Software

Le libre est un mouvement qui s'intéresse principalement aux logiciels informatiques.


## Quelques licences d'utilisation

* Creative Commons

## À ne pas confondre avec...

* Domaine public

## Le rôle du libre dans la création en arts

## Le libre comme multiplicateur de la création

* Processing
* p5


## Le libre comme oeuvre en soit

* esolangs: langages de programmation ésotériques
* outils d'art génératif ( in:verse )

## Le libre et son écosystème

* Gitlab
* GitHub
* sites internet de partage de sons freesound.org

## Le libre et ses communautés

# Le libre et la SAT

Place à la discussion !

