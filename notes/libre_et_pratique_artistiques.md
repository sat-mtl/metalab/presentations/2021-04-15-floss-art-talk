Ce qu'apporte le libre dans la pratique artistique
==================================================

Contexte juridico-légal
-----------------------

Par défaut, toute création (image, son, vidéo, texte, code, ...) diffusée sans licence est considéré propriétaire, sans droit de modification ou de copie.

Les licences libres visent à préciser les droits d'utilisation, partage et modification des logiciels et autres créations.

Les licences libres et le partage
---------------------------------

* licences logicielles
    * GPL
    * LGPL, AGPL
    * MIT, BSD, MPL, ...
* licences de contenu
    * Creative Commons

À propos de la propriété de ses créations
-----------------------------------------

### Où s'arrête la création ?

* la création doit-elle s'arrêter à ce qui est rendu possible par un logiciel
* la possibilité d'améliorer/modifier un logiciel augmente la possibilités de création

### Un outil pour améliorer la pérennité des arts numériques

Sur la base de :
* les outils propriétaires fonctionnent de plus en plus sur la base d'abonnements
* la licence donne un droit d'utilisation des logiciels, notre copie ne nous appartient jamais. Cette licence peut être révoquée à tout moment (qui lit les EULA ?)
* les logiciels propriétaires poussent des formats de fichier fermés
* il est difficile d'accéder à d'anciennes versions des logiciels propriétaires
* certains logiciels forcent la mise à jour
* certaines plateformes ont en plus le bon goût de stocker les créations sur les serveurs des éditeurs de logiciel

Le créateur n'est donc pas assuré de pouvoir ouvrir les fichiers contenant leurs création, et cette probabilité diminue avec le temps. Il n'est donc pas en contrôle de l'accès à ses contenus, on peut questionner le fait qu'il soit effectivement propriétaire de ses création. Cette question est d'autant plus valide lorsque les créations sont stockées sur des serveurs distants (encore une fois, des surprises existent dans les EULA de certaines plateformes).

Avec des logiciels libres, le créateur est assuré d'avoir accès a minima au code source d'un logiciel. Dans le pire des cas il sera donc possible de convertir les fichiers de contenu, ou d'adapter le logiciel à un environnement actuel. L'accès aux anciennes version est également assuré de par l'accès a minima à l'historique du code source.

Le fait d'être en contrôle du logiciel et d'être assuré de ne pas en perdre l'accès donne au créateur la totalité du contrôle sur ses propres créations. Il n'est alors dépendant d'aucune structure externe pour pouvoir y accéder, et par conséquent est le seul propriétaire de ses contenus.

Pour maintenir une œuvre numérique dans le temps, il faut prendre en compte :
* la disponibilité des logiciels impliqués
* la disponibilité du matériel
* l'accessibilités des contenus

Les licences libres facilitent la disponibilité des logiciels sur le long terme. Par exemple le créateur peut conserver une copies des logiciels et de leur code source en annexe à leurs créations. Il leur est également possible (directement ou indirectement) de participer au maintien de ces logiciels dans le temps pour assurer leur disponibilité.
