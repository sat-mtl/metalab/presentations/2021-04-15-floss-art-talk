
# End-user programming


> What if we, and all computer users, could reach in and modify our favorite apps? Or even create new apps on the fly according to our needs in the moment?

End-user programming was a (utopic?) vision of empowered users pursued by computer science visionaries of the 60s.

The terminal is a quintessential end-user programming environment.
A spreasheet is another.

> There is a moral imperative for end-user programming: to avoid a digital divide.

-   <https://www.inkandswitch.com/end-user-programming.html>
