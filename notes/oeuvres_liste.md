# Listes - oeuvres intéressantes et inspirantes

## Oeuvres
[List of major Creative Commons licensed works](https://en.wikipedia.org/wiki/List_of_major_Creative_Commons_licensed_works)

### Passage

released as public domain in 2007

http://hcsoftware.sourceforge.net/passage/

Credits:

    All code, music, graphics, fonts, and animation by me.
    I used mtPaint to make all of the pixel art.
    I used the SDL Library for cross-platform screen, sound, and user input.
    I used MinGW to build the game for Windows. 

### xkcd

xkcd.com

### Yoko Ono

https://creativecommons.org/2009/10/14/yoko-onos-plastic-ono-band-goes-cc/

### PRÖSPECT

http://archiverlepresent.org/fiche-de-la-collection/prospect

### Next Gen

Par [Tangent Animation](https://www.tangent-animation.com/next-gen)

### Suivi de partition - IRCAM

Patch Puredata pour les oeuvres de Pierre Boulez, Karlheinz Stockhausen, Philippe Manoury, Rand Steiger, années 80. http://msp.ucsd.edu/pdrp/latest/doc/
