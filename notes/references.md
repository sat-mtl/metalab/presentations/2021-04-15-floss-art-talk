# Ressources et références

## Pérennité

https://timreview.ca/article/294

https://github.com/antimodular/Best-practices-for-conservation-of-media-art

https://www.stereolux.org/blog/conserver-une-oeuvres-numerique-des-creations-deja-en-voie-de-disparition

https://www.dpconline.org/handbook/technical-solutions-and-tools/file-formats-and-standards

https://www.canada.ca/fr/reseau-information-patrimoine/services/preservation-numerique/recommandations-formats-fichier-preservation-numerique.html

https://www.bnf.fr/fr/formats-de-donnees-pour-la-preservation-numerique

## Générales

https://www.vice.com/en/article/wnzm4q/how-open-source-is-disrupting-visual-art

http://chatonsky.net/libre-2/

https://en.wikipedia.org/wiki/List_of_major_Creative_Commons_licensed_works


