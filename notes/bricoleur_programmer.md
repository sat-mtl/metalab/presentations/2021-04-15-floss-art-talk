
# Bricolage programming


Alex McLean and Geraint Wiggins present an emergent approach to artistic creation that is manifested in writing computer code to create audio or visuals. They examine the "relationship between an artist, their creative process, their program and their artistic works". They clarify the possible definition of *bricolage* approach to programming might be via a quote from Turkle and Papert [1992] that show it through an analogy with painter's creative feedback loop. The *bricoleur* programmer is juxtaposed with a *planner*. Planning is considered to be rigid while bricolage more intuitive and flexible. They draft an evolution of programming environments that cater to artists.

-   McLean, A., & Wiggins, G. (). Bricolage Programming in the Creative
    Arts. , (), 11.

-   Sherry Turkle and Seymour Papert. Epistemological pluralism: Styles and voices within the

computer culture. Signs, 16(1):128–157, 1990. ISSN 00979740. doi: 10.2307/3174610.
