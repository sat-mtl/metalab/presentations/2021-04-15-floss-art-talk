### Le logiciel libre et open source: un standard de fait

L'industrie informatique se structure de plus en plus en fonction de ce standard de fait. Par exemple, Microsoft a acheté Github.com pour agir en tant que force fédératrice d'un immense patrimoine mondial qui se contitue grâce aux logiciels libres et open source.

### Les administrations publiques adoptent le libre

Le logiciel libre et open source peut être considéré comme une manière de standardiser les pratiques de développement et d'utilisation des logiciels. Plusieurs administrations publiques l'ont adoptés, soit comme en tant que faisant partie de l'offre, soit en tant que critère de sélection, en tant que standard.

La ville de Montréal a défini sa politique logicielle (acquisition et développement) en fonction de toutes les offres disponibles, de propriétaires à libres: http://ville.montreal.qc.ca/pls/portal/docs/PAGE/PRT_VDM_FR/MEDIA/DOCUMENTS/politique_materiel_libres_fr.pdf
Montréal publie aussi de pus en plus de données ouvertes: https://donnees.montreal.ca/

L'Allemagne a un historique intéressant quant à l'adoption du libre dans l'administration publique, en particulier la ville de Munich qui a adopté le "libre" il y a une quinzaine d'année pour revenir à Windows en 2020, une décision politique prise en 2017 suite à un changement de gouvernance. Une campagne est en cours depuis 2 ans pour sensibilier les administrations publiques: https://fsfe.org/news/2019/news-20190515-01.html

Récemment la ville de Dortmund a fait du logiciel libre un standard obligatoire pour toute l'administration publique: 
https://blog.documentfoundation.org/blog/2021/04/02/free-software-becomes-a-standard-in-dortmund-germany/

De manière générale, pour tout ceux qui adopte le "libre", il s'agit d'abord d'un enjeu d'autonomie:
https://www.asafrance.fr/item/autonomie-strategique-les-logiciels-libres-interessent-les-etats.html

La France aussi adopte le libre, par exemple pour la messagerie:
https://matrix.org/blog/2018/04/26/matrix-and-riot-confirmed-as-the-basis-for-frances-secure-instant-messenger-app
et la gendarmerie:
http://www.profession-gendarme.com/le-logiciel-libre-a-la-gendarmerie-francaise-1-alibi-pour-une-independance-technologique/

Cette mouvance peut s'observer ici:
https://joinup.ec.europa.eu/collection/open-source-observatory-osor


