set -e

if [ ! -d reveal.js ]
then
    wget https://github.com/hakimel/reveal.js/archive/3.9.2.tar.gz
    tar -xzvf 3.9.2.tar.gz
    mv reveal.js-3.9.2 reveal.js
fi

pandoc -t revealjs --css styles.css --metadata pagetitle="Arts et (logiciel) libre" -s -o presentation.html presentation.md -V revealjs-url=./reveal.js --variable transition='none' --variable theme="moon"

