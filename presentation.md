---
title:
- Le libre et les arts
author:
- Edith Viau, Michał Seta, Nicolas Bouillot et le Metalab
institute:
- Société des Arts Technologiques [SAT]
theme:
- default
date:
- 22 avril 2021
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

# Structure de la présentation

* Contexte de la présentation (Edith)
* Tour d'horizon de logiciels libres et d'oeuvres d'art réalisées avec ceux-ci (Edith)
* Le libre et la pratique artistique (Michał)
* Le libre à la SAT (Nicolas)
* Discussion

# Concepts-clefs

* appropriation des logiciels et de la production informatique-numérique par l'artiste
* pérennité des oeuvres, des projets
* co-création entre l'artiste-utilisateur et le développeur-contributeur (qui peuvent être la même personne)
* démocratisation  et accessibilité des outils

# À la découverte du libre

première partie

#

>"One of my beliefs of the time was that artists rise to the level of technology they have access to. And technology rises to the level it has access to artists."
>
> -- <cite>citation de William Perry, dans *Around Computerese: the Electronic Media Magazine* [Artexte](https://artexte.ca/2017/11/artexte-on-air-entourant-computerese/)</cite>

# À la découverte du libre

Commençons par explorer l'univers du logiciel libre...

# Que veut-on dire par "libre" ?

4 vérités: la personne-utilisatrice a les libertés suivantes :

- utiliser le logiciel
- copier le logiciel
- étudier le logiciel
- modifier le logiciel et distribuer les versions modifiées

# Les logiciels libres

Voici quelques logiciels libres populaires - les reconnaissez-vous ?

# Blender

![](images/blender_composite.png)

# Pure Data

![](images/pure_data.png){width=80%}

# darktable

![](images/darktable.jpg){width=80%}

# ... et quelques autres...

- Audacity
- Processing
- openFrameworks
- SuperCollider
- Gimp
- Firefox
- Linux

# ... et quelques autres...

En connaissez-vous d'autres ? Nommez-les dans le *chat* !

# À la découverte du libre: les oeuvres

Partons maintenant à la découverte de certaines oeuvres réalisées avec ces logiciels.

# Phantogram - Fall in Love

<iframe src="https://player.vimeo.com/video/85263005" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/85263005">glitch 041 / Phantogram / Fall in Love</a> from <a href="https://vimeo.com/joshuadavis">Joshua Davis</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

# Christian Höller - Video_64CDE

[Site web de l'artiste](http://dextro.org/dddd/064V/D064.html)

<iframe src="https://player.vimeo.com/video/104033579?color=ffffff&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/104033579">064d</a> from <a href="https://vimeo.com/dextroorg">dextroorg</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

# Christian Höller - Video_64CDE

>"Dextro writes ‘non-linear code’ drawing inspiration from nature. The results are non-fractal or random programs that iterate without change, with equal rules for all objects."
>
> -- <cite>citation prise du [site web de Processing](https://processing.org/exhibition/curated_page_14.html)</cite>

# Phillip Stearns - Fragmented Memory

> "Three selections of the binary data were converted to images using custom software written with the help of Jeroen Holthuis in Processing which grouped 6 bits into RGB pixel color values (2 bits per channel)."
>
> -- <cite>Phillip Stearns, citation prise de son [site web](https://phillipstearns.wordpress.com/fragmented-memory/)</cite>

# Phillip Stearns - Fragmented Memory

<iframe src="https://player.vimeo.com/video/71044541" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/71044541">Fragmented Memory Process (Edited for Wired 2013)</a> from <a href="https://vimeo.com/phillipstearns">Phillip Stearns</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

# Radiohead - House of Cards

Le groupe Radiohead a rendu accessible sous licence Creative Commons les données d'animation utilisées pour le vidéo de la chanson *House of Cards*.

# Radiohead - House of Cards

![](images/radiohead.png)

# Niklas Roy - My little piece of Privacy

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/rKhbUjVyKIc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Niklas Roy - My little piece of Privacy

> "For better performance, I also re-wrote the computer vision software in C++ (with the use of OpenFrameworks)."
>
> -- <cite>Niklas Roy, citation prise de son [site web](https://www.niklasroy.com/project/88/my-little-piece-of-privacy)</cite>

# Le libre dans la pratique artistique

deuxième partie

# Un peu de contexte...

C'est quoi le libre?

# Un peu de contexte...

Pourquoi se poser la question?

# la propriété intellectuelle (PI)

- ne protège pas les idées mais l'expression des idées

# Historique

- ~560, Saint Colomban d'Iona et Saint Finnian
- avant XVIII siècle: droit d'auteur lié au monopole
- XVIII siècle: l'auteur a le droit exclusif pour imprimer (21 ans)
- XIX siècle: droit d'auteur international et droit moral

# Le numérique...

La reproduction est facile. Comment faire?

# Logiciel et la PI

- le code source d'un logiciel est l'**expression** des idées
- USA a développé des lois très agressives pour la protection du logiciel

# Logiciel propriétaire

- jouit des loi qui permettent de cacher les détails de son invention

> “For the first time since Sybaris, 500 B.C., who imposed public disclosure in exchange for the legal protection of recipes, private property and secret are reconciled!”
>
> -- <cite>M. Vivant, 1993, Une épreuve de vérité pour les droits de propriété intellectuelle : le développement de l’informatique, in L’avenir de la propriété intellectuelle, Librairies Techniques, Collection «Le Droit des Affaires», Paris.</cite>

# Logiciel libre

4 vérités: la personne-utilisatrice a la liberté de:

- utiliser le logiciel
- copier le logiciel
- étudier le logiciel
- modifier le logiciel et distribuer les versions modifiées

# Création artistique et le numérique

> "Programmability is a unique property of the medium. It is a high level property that is not inherent in the digital information itself".
>
> -- <cite>Kilian, A. Defining Digital Space Through a Visual Language.</cite>

# Programmation pour l'utilisateur final

> "What if we, and all computer users, could reach in and modify our favorite apps? Or even create new apps on the fly according to our needs in the moment?"

> "There is a moral imperative for end-user programming: to avoid a digital divide".
>
> -- <cite>[site web de Ink & Switch](https://www.inkandswitch.com/end-user-programming.html)</cite>

# Bricolage programming

- Programmer - approche similaire à l'improvisation dans le contexte de la création

> -- <cite>McLean, A., & Wiggins, G. (). Bricolage Programming in the Creative Arts.</cite>
>
> -- <cite> Sherry Turkle and Seymour Papert. Epistemological pluralism: Styles and voices within the computer culture. Signs, 16(1):128–157, 1990. ISSN 00979740. doi: 10.2307/3174610.</cite>

# Les limites de la création

- Peut-elle aller au delà du logiciel?

- La possibilité d'améliorer/modifier un logiciel augmente la possibilités de création

# Pérennité - problèmes

> - Un logiciel peut cesser d'exister (LogicAudio (Windows), Studio Vision)
> - Un format de fichier propriétaire aussi
> - Certains logiciels forcent la mise à jour
> - Les licences expirent
> - Si on n'est jamais propriétaire d'un logiciel - est-ce qu'on est *vraiment* propriétaire de l'oeuvre créée avec ce logiciel?

# Pérennité - solutions

> - Être en contrôle de ses outils de création
> - Accès au code source peut aider à restaurer, améliorer, faire évoluer une oeuvre numérique
> - Accès aux implémentations/spécifications des formats de fichiers peut aider à la migrations des contenus vers les formats actuels


# We are not alone!

Fresh off the press: https://tinytools.directory/


# La SAT et le libre

troisième partie

# Déjà en 2005
> "Le cœur du concept de hub urbain [...] consiste à offrir une plateforme ouverte et évolutive qui réunit des logiciels d’avant-garde (essentiellement en code source libre), [...] qui permettent la création, la diffusion et la distribution de contenu culturel numérique."
>
> -- <cite>Le Hub urbain de la SAT. Husband et Barsalo. 2005</cite>.

# Des contributions de la SAT

- Tous les **logiciels du Metalab** depuis 2002
- **Jeu de donnés OSM** publié sous licence Creative Commons
- **Matériel libre** (Audiodice & Mirador)
- Contributions à des **logiciels tiers** (incluant Blender, Supercollider, PyOpenPose et FAUST)

# Résidences & libre -- série Contamine

>"Le projet Contamine est un programme d’échanges artistiques nationaux et internationaux ayant pour objectif de réaliser des résidences croisées impliquant la recherche et la création multidisciplinaire."
>
>[Résidence Contamine avec Scenic (2008-2014)](https://sat.qc.ca/fr/residences/contamine)

![](images/Scenic_RGB_horizontal.png){width=30%}

# Les résidences Contamine avec le logiciel Scenic

- 2008-2011 [Soupe transatlantique avec Mons](https://sat.qc.ca/fr/evenements/soupe-transatlantique)
- 2011-2012 [Lucifuge avec Poitiers](https://sat.qc.ca/fr/nouvelles/lucifuge)
- 2012 [Taïwan](https://archives.sat.qc.ca/fr/content/contamine-taiwan-2012)
- 2012-2013 [Drawing Space avec Liverpool](https://sat.qc.ca/fr/evenements/lespace-dessiner-drawing-space)
- 2013-2014 [Miscible avec Rennes](https://sat.qc.ca/fr/miscible)

# Miscible, une des 5 résidences Contamine

<iframe src="https://player.vimeo.com/video/106639761?color=ffffff&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/106639761">Miscible</a> from <a href="https://vimeo.com/satmontreal">Society for Arts and Technology</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->

# Résidences & libre -- Scènes ouvertes
[20+ lieux](https://sat.qc.ca/fr/scenic-telepresence) pour [environ 10 projets de téléprésence](https://sat.qc.ca/fr/nouvelles/evenements-en-telepresence-scenique-0) par an, et le réseau gagne des [prix](https://sat.qc.ca/fr/nouvelles/deux-prix-pour-des-oeuvres-en-telepresence) !

![](./images/carte_scenesouvertes_site_noirerouge.png){width=60%}

# Corpstempslieux

<iframe src="https://player.vimeo.com/video/369842091" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/369842091">CorpsTempsLieux</a> from <a href="https://vimeo.com/satmontreal">Society for Arts and Technology</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->

*Prix Arts visuels Centre-du-Québec*

# Outils du Metalab & Artistes

Téléprésence, théâtre, musique vivante, dôme

- 2011 [INTACT, Just In Case](http://saramalinarich.net/en/just-in-case-2/)
- 2013 [Dieu est un DJ](https://sat.qc.ca/fr/evenements/dieu-est-un-dj-residence)
- 2013 [Waterfall music](https://nicolasbouillot.net/projects.php?id=35)
- 2014 Réseau natif


# Réseau natif - Téléprésence entre dômes !

![](images/scenic-IAIA-2014-IX.jpg){width=70%}

_SAT et IAIA de Santa Fe durant le symposium iX 2014_

# Outils du Metalab & Artistes

Architecture, maquette, piscine, acrobatie, installation, interaction, dôme

- 2014 [Turcot, la route devenue architecture](https://sat.qc.ca/fr/evenements/turcot-la-route-devenue-architecture)
- 2016 [Aquakhoria](https://sat.qc.ca/en/evenements/aqua-khoria)
- 2018 École Nationale de Cirque
- 2019-2020 [Co-création espace immersif](https://vimeo.com/345694399)
- 2020 [Navigation dans l'OSM](https://vimeo.com/522509890)
- 2021 Molécule / -22.7

# Turcot, la route devenue architecture

<iframe src="https://player.vimeo.com/video/354719487" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/354719487">Turcot : la route devenue architecture</a> from <a href="https://vimeo.com/satmontreal">Society for Arts and Technology</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->

# Actions !

- Améliorer la **documentation** et communications propice aux **contributions externes**
- Créer un **axe d'enseignement orienté recherche et libre** à Campus
- Créer des **programmes de résidences** autour des outils libres
- **Positionner activement la SAT** vis-à-vis du libre

# Concepts-clefs --- rappel

* appropriation des logiciels et de la production informatique-numérique par l'artiste
* pérennité des oeuvres, des projets
* co-création entre l'artiste-utilisateur et le développeur-contributeur (qui peuvent être la même personne )
* démocratisation  et accessibilité des outils

# Conclusion

Merci pour votre écoute !

# Références

## Logos et images

* Pure Data: [Wikimedia](https://commons.wikimedia.org/wiki/File:Pure_Data_with_many_patches_open_(showing_netpd_project).png)
* darktable: [site web de l'application](https://www.darktable.org/about/screenshots/screenshot_darkroom2.jpg)
* Radiohead: [site web de Creative Commons](https://creativecommons.org/2009/11/02/printing-thom-yorkes-head/)
