# À la découverte du libre

Le monde du logiciel libre est fascinant et intriguant, mais il est plus près de nous que l'on ne le pense !

Commençons par explorer l'univers du logiciel libre...

# Les logiciels libres

Voici quelques logiciels libres populaires - les reconnaissez-vous ?

# Blender

Blender est utilisé pour la modélisation, l'animation et la modélisation en trois dimensions.

# Pure Data

Pure Data permet de réaliser des créations audio et multimédia à partir d'une interface graphique.

# darktable

Ce logiciel libre faciliter le traitement d'images pour la photographie numérique.

# SuperCollider

Environnement et langage de programmation développé pour la synthèse sonore en temps réel ainsi que pour la composition algorithmique.

# Audacity

Ce logiciel permet d'enregistrer et d'éditer des sources audionumériques.

# Processing

Cet environnement de développement vise à simplifier la programmation pour les oeuvres numériques et multimédia, ainsi que pour le graphisme.

# openFrameworks

Cet ensemble d'outils pour la création numérique ("creative coding") est inspiré de Processing - le premier est écrit en C++, le second en Java.

# À la découverte du libre: les oeuvres

Alors, jusqu'où peut-on aller avec ces logiciels ? Regardons quelques oeuvres choisies parmis la gallerie de Processing...

# Phantogram

Ces graphiques ont été réalisés avec Processing afin de faire partie d'un vidéo pour la chanson Fall in Love, du groupe Phantogram.

On voit beaucoup d'effets de symétries, du graphisme inspiré de la géométrie euclidienne qui rencontre des fractales, beaucoup de mouvement et de rhythme.

# Christian Höller

Dans cette oeuvre, c'est plutôt un flux continue de changement qui nous attend, avec beaucoup d'attention au détail - bien que le site de Processing mentionne que le code derrière l'oeuvre est bref.

# Phillipe Stearns

Ici, on est dans l'art au sujet du numérique, et un peu dans l'histoire - les métiers à tisser de type Jacquard étant les premières machines programmables à l'aide de cartes perforées, début 1800.

# Nikolas Roy

Cette oeuvre ludique nous ramène au point mentionné par Perry au début de notre présentation: les artistes s'élèvent à la hauteur de la technologie qui leur est accessible...

... et la technologie s'élève en ayant accès à l'art.


